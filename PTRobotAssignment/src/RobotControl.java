import javax.swing.*;
import java.util.Scanner;

class RobotControl
{
   private Robot r;
   public RobotControl(Robot r)
   {
       this.r = r;      // Variable used to reference the robots actions.
   }

   public void control(int barHeights[], int blockHeights[])
   {
     //sampleControlMechanism(barHeights,blockHeights);
     
     // controlMechanismForScenarioA(barHeights, blockHeights);
     // controlMechanismForScenarioB(barHeights, blockHeights);
      controlMechanismForScenarioC(barHeights, blockHeights);
       
     // camsControls();
       
   }

     /* #############################################################################
        ### CAMOS MANUAL ROBOT MOVEMENT TESTING METHOD                            ###
        ### THIS METHOD IS USED SOLELY FOR TRYING TO OBTAIN THE MINIMUM MOVES     ###
        ### AND WORKING OUT SOLUTIONS AS TO HOW TO CODE THESE SOLUTIONS           ###
        #############################################################################*/
   
    final static Scanner keyPress = new Scanner(System.in);
   
    public void camsControls()
    {
        int response;

        do
        {
            /* CONTROLS - ENTER THESE INTO THE CONSOLE (I SUGGEST USING THE KEYPAD)

            7 ARM3      8 ARM1      9 ARM3
            RAISE        RAISE       LOWER

            4 ARM2      5 ARM1      6 ARM2
            CONTRACT     LOWER      EXPAND

            1 PICKUP                3 DROP
            BLOCK                   BLOCK */

            System.out.print("Enter selection: ");
            while (!keyPress.hasNextInt())
            {
                keyPress.next(); // this is important!
            }
            response = keyPress.nextInt();

            System.out.println();

            switch (Integer.valueOf(response))
            {
            	// ARM1 MOVEMENTS
                case 8:
                    r.up();
                    break;

                case 5:
                    r.down();
                    break;

                // ARM2 MOVEMENTS
                case 4:
                    r.contract();
                    break;

                case 6:
                    r.extend();
                    break;

                // ARM3 MOVEMENTS
                case 7:
                    r.raise();
                    break;

                case 9:
                    r.lower();
                    break;

                // PICKUP/DROP MOVEMENTS
                case 1:
                    r.pick();
                    break;
                   
                case 3:
                    r.drop();
                    break;


                default:
                    System.out.println("Error - invalid selection!");
            }
            System.out.println();

        }
        while (response != 0);
    }


    public void sampleControlMechanism(int barHeights[], int blockHeights[])
    {
    // Internally the Robot object maintains the value for Robot height(h), 
         // arm-width (w) and picker-depth (d).

         // These values are displayed for your convenience
         // These values are initialized as h=2 w=1 and d=0

         // When you call the methods up() or down() h will be changed   
         // When you call the methods extend() or contract() w will be changed   
         // When you call the methods lower() or raise() d will be changed   


         // sample code to get you started
         // Try running this program with obstacle 555555 and blocks of height 2222 (default)
         // It will work for first block only 
         // You are free to introduce any other variables


         int h = 2;         // Initial height of arm 1
         int w = 1;         // Initial width of arm 2  
         int d = 0;         // Initial depth of arm 3

         int sourceHt = 12;      

         // For Parts (a) and (b) assume all four blocks are of the same height
         // For Part (c) you need to compute this from the values stored in the 
         // array blockHeights
         // i.e.  sourceHt = blockHeights[0] + blockHeights[1] + ...  use a loop!
     
//       int targetCol1Ht = 0;    // Applicable only for part (c) - Initially empty
//       int targetCol2Ht = 0;    // Applicable only for part (c) - Initially empty

         // height of block just picked will be 3 for parts A and B
         // For part (c) this value must be extracting the topmost unused value 
         // from the array blockHeights

         int blockHt = 3;      


         // clearance should be based on the bars, the blocks placed on them, 
         // the height of source blocks and the height of current block

         // Initially clearance will be determined by the blocks at source (3+3+3+3=12)
         // as they are higher than any bar and block-height combined 

         int clearance = 12;  

         // Raise it high enough - assumed max obstacle = 4 < sourceHt 
         while ( h < clearance + 1 ) 
         {
             // Raising 1
             r.up();     

             // Current height of arm1 being incremented by 1
             h++;
         }

         System.out.println("Debug 1: height(arm1)= "+ h + " width (arm2) = "+
                            w + " depth (arm3) =" + d); 

         // this will need to be updated each time a block is dropped off
         int extendAmt = 10;

         // Bring arm 2 to column 10
         while ( w < extendAmt )
         {
            // moving 1 step horizontally
            r.extend();

            // Current width of arm2 being incremented by 1
            w++;
         }

         System.out.println("Debug 2: height(arm1)= " + h + " width (arm2) = "+
                            w + " depth (arm3) =" + d); 

         // lowering third arm - the amount to lower is based on current height
         //  and the top of source blocks

         // the position of the picker (bottom of third arm) is determined by h and d
         while ( h - d > sourceHt + 1)   
         {
            // lowering third arm
            r.lower();

            // current depth of arm 3 being incremented
            d++;
         }


         // picking the topmost block 
         r.pick();

         // topmost block is assumed to be 3 for parts (a) and (b)
         blockHt = 3;

         // When you pick the top block height of source decreases   
         sourceHt -= blockHt;

         // raising third arm all the way until d becomes 0
         while ( d > 0)
         {
             r.raise();
             d--;
         } 

         System.out.println("Debug 3: height(arm1)= " + h + " width (arm2) = "+
                            w + " depth (arm3) =" + d); 

         // why not see the effect of changing contractAmt to 6 ? 
         int contractAmt = 7;

         // Must be a variable. Initially contract by 3 units to get to column 3
         // where the first bar is placed (from column 10)

         while ( contractAmt > 0 )
         {
             r.contract();
             contractAmt--;
         }

         System.out.println("Debug 4: height(arm1)= " + h + " width (arm2) = "+
                            w + " depth (arm3) =" + d); 


         // You need to lower the third arm so that the block sits just above the bar
         // For part (a) all bars are initially set to 7
         // For Parts (b) and (c) you must extract this value from the array barHeights

         int currentBar  = 0;             

         // lowering third arm
         while ( (h - 1) - d - blockHt > barHeights[currentBar] )   
         {
             r.lower();
             d++;
         }

         System.out.println("Debug 5: height(arm1)= " + h + " width (arm2) = "+
                            w + " depth (arm3) =" + d); 
         
         // dropping the block      
         r.drop();

         // The height of currentBar increases by block just placed    
         barHeights[currentBar] += blockHt;

         // raising the third arm all the way
         while ( d > 0 )
         {
             r.raise();
             d--;
         }
         System.out.println("Debug 6: height(arm1)= " + h + " width (arm2) = " +
                            w + " depth (arm3) =" + d); 

         // This just shows the message at the end of the sample robot run -
         // you don't need to duplicate (or even use) this code in your program.

         JOptionPane.showMessageDialog(null,
                                       "You have moved one block from source " +
                                       "to the first bar position.\n" + 
                                       "Now you may modify this code or " +
                                       "redesign the program and come up with " +
                                       "your own method of controlling the robot.", 
                                       "Helper Code Execution", 
                                       JOptionPane.INFORMATION_MESSAGE);
         // You have moved one block from source to the first bar position. 
         // You should be able to get started now. 
    }
   
   
    public void controlMechanismForScenarioA(int barHeights[], int blockHeights[])
    {
        // ##############################################################################
        // ###        Original Solution written for Part A of the Assignment          ###
        // ###                        Also worked for Part B                          ###
        // ###                               Version 1                                ###
        // ##############################################################################
       
        //DECLARE VARIABLES FOR USE WITHIN PROGRAM
        int h = 2;         // Initial height of arm 1
        int w = 1;         // Initial width of arm 2  
        int d = 0;         // Initial depth of arm 3

        int sourceHt = 12;    // Total Height of the source pile
        int blockHt = 3;      // Height of Blocks to be lifted ONLY FOR parts A and B. Will change this to a variable in future

        int clearance = 12;    // Lowest height arm 2 can be to CLEAR the blocks in piles 1-9
        int extendAmt = 10;    // Variable to return the arms to the Source pile in column 10
         
        int currentBar  = 0;   // Currently selected BAR within the barHeights[] ARRAY

        int contractAmt = 7;   // Variable to control the current STACK to be filled, also used to return the arms to that STACK after a new block has been picked up

        while ( h < clearance + 1 ) 
        {
            // Raising the control arm up by 1 unit
            r.up();     

            // Current height of arm1 being incremented by 1
            h++;
        }

        while (sourceHt != 0)    // Repeating the steps to move the arms and pickup a block for EACH block within the blockHeights ARRAY
        {   
            while (w < extendAmt)  // Causes the arm to always return to source pile
            {
                r.extend(); // Moving arm2, 1 unit horizontally
                w++;    // Current width of arm2 being incremented by 1
            }

            while (h - d > sourceHt + 1)   // Lowering the 3rd arm (Grapple Hook) to allow it to pickup a block
            {
                r.lower();
                d++;    // Current depth of arm 3 being incremented
            }

            // Picking the topmost block 
            r.pick();

            // When you pickup the top block, the height of source pile decreases, so the sourceHt Variable needs to reflect this   
            sourceHt -= blockHt;

            // Raising third arm all the way up until d becomes 0 (IE until the Grapple hook has retracted fully)
            while (d > 0)
            {
                r.raise();
                d--;   // Current depth of arm 3 being decreased
            } 

            while ( w > (10-contractAmt) )  // Contract arm2 to allow placement of the newly picked up block
            {
                r.contract();
                w--;   // Current width of arm2 being decreased
            }
            contractAmt--; // Now that the Column has a Block on the Bar it is now FULL (Per Assignment Restrictions). Adjust the contractAmt so that the next block will be placed in the next column         

            while ( h > barHeights[currentBar] + blockHt + 1 ) 
            {
                // Raising the control arm up by 1 unit
                r.down();     

                // Current height of arm1 being decreased by 1
                h--;
            }      

            // lowering third arm to the Height of the Bar in current column MINUS the height of the current block
            while ( (h - 1) - d - blockHt > barHeights[currentBar] )   
            {
                r.lower();
                d++;   // Current depth of arm 3 being decreased
            }
             
            // dropping the block      
            r.drop();

            // The height of currentBar increases by block just placed    
            barHeights[currentBar] += blockHt;
             
            currentBar++;      // Shift to the next bar in the array barHeights[]
           
            // raising the third arm all the way
            while ( d > 0 )
            {
                r.raise();
                d--;
            }
         
        } // close while loop (Looping the arm movements for EVERY block)
    } // public void controlMechanismForScenarioA
   
   
    public void controlMechanismForScenarioB(int barHeights[], int blockHeights[])
    { /*
        // #######################################################################################
        // ### HEAVILY Modified version of original solution above, mostly due to adding main  ###
        // ###       items to Methods to allow code re-use across this solution                ###
        // ###                                   Version 10                                    ###
        // #######################################################################################
       
        // Working for Part A + B  NOT C, as it CANNOT sort by blockHt
       
        // ### 1 ### DECLARE VARIABLES
        // Declaration of all variable used within this program 
        int height = 2;        // Initial height of arm 1
        int width = 1;         // Initial width of arm 2  
        int depth = 0;         // Initial depth of arm 3

        int sourceHt = 0;      // Variable to show the height of the Source Pile

        int highestBar = 0;     // Stores the highest bar from the array blockHeights[]
        int highestUnfilledBar = 0;  // ####################################################################
        int clearance = 0;      // Lowest height arm 2 can be to CLEAR the blocks in ALL piles

        final int SOURCE_COLUMN = 10;     // Column number of the Source Pile

        int targetColumn = 0;  // Next column to drop-off block
        int fullColumns = 2;   // Columns that already have a block in them (for Columns 3-9)

        int currentBlockHt = 0; // Height of the current block to be lifted
        int currentBar  = 0;    // ID Number of Currently selected BAR within the barHeights[] ARRAY
        int currentBlock = 0;   // ID Number of Currently selected BLOCK within the blockHeights[] ARRAY


        // ### 2 ### CALCULATE sourceHt
        // Calculating the sourceHt variable, by reading through the array and adding each element
        // of the blockHeights array, one element at a time 
        while (currentBlock < blockHeights.length)
        {
          sourceHt = sourceHt + blockHeights[currentBlock];
          currentBlock++;
        }
        currentBlock--;  // Once the array has be read, this variable will equal the total length of the array (ie 6)
                         //   but when referencing the array, the elements are 0-5

     
        // ### 3 ### CALCULATE highestBar
        // Calculate the height of the highest bar, by reading through the array and comparing each element 
        highestBar = calculateHighestBar(currentBar, barHeights);


        // ### 4 ### RAISE ARM1 TO CORRECT HEIGHT
        //   Raise height to clear the highest point (either the source pile or highestBar) 
        if (sourceHt > highestBar)
        {
          clearance = sourceHt + 1;  // + 1 (to allow for arm2's height)
        }
        else
        {
          clearance = highestBar + 1;  // + 1 (to allow for arm2's height)
        }

        height = clearanceAdjustment(height, clearance); // Method used to raise arm1 to the newly calculated height


        // MAIN PROGRAM LOOP
        while (sourceHt != 0)
        {

            // ### 5 ### - PICKUP NEXT BLOCK FROM SOURCE 
            //   Extend to source pile 
            while (width < SOURCE_COLUMN)
            {
                r.extend();
                width++; 
            }

            // Lower grapple to top of source pile
            // (Height of arm1) - (depth of grapple arm3) - 1 (for height of arm2) > (Height of source pile)
            while (height - depth - 1 > sourceHt)
            {
                r.lower();
                depth++; 
            }
  
            r.pick(); // Picking the topmost block 

            // Calculate the height of the newly acquired block
            currentBlockHt = blockHeights[currentBlock];

            // When you pickup the top block, the height of source pile decreases, so the sourceHt Variable needs to reflect this   
            sourceHt = sourceHt - currentBlockHt;

            // Move arm2 one space to the left, before adjustments (which prevents any collisions with the source pile)
            r.contract();
            width--;


            // ### 6 ### - CALCULATE AND ADJUST CLEARANCE
            //   This adjust clearance is used mostly when moving blocks of height 1 or 2, to make sure that they can clear the highest bar or column 

            //highestUnfilledBar = calculateHighestBar(highestBar, currentBar, barHeights);
            highestUnfilledBar = calculateHighestBar(currentBar, barHeights);
            
            // If the bars on the left are currently greater than the source + current block, then raise arm
            clearance = calculateClearance (currentBlockHt, clearance, highestBar, highestUnfilledBar);

            height = clearanceAdjustment(height, clearance);



            // Raising the grapple, if it is likely to collide with the highestUnfilledBar or the Source Pile,
            if (height - depth > (highestUnfilledBar + currentBlockHt + 1))
            {
                while (height - depth > (highestUnfilledBar + currentBlockHt + 1)) 
                {
                    r.lower();
                    depth++;
                }
            }
            else 
            {
                while (height - depth < (highestUnfilledBar + currentBlockHt + 1))
                {
                    r.raise();
                    depth--;
                } 
            }   


            // ### 7 ### - CALCULATE AND MOVE TO TARGET COLUMN
            // Uses the currentBlockHt to determine where to place the block. 
            // Size 3 go into columns 3 - 9, calculates correct column to place   
                targetColumn = fullColumns + 1;

            while (width > targetColumn)  
            {
                r.contract();
                width--;
            }


            // ### 8 ### - CALCULATE AND ADJUST CLEARANCE
            //  Calculate Clearance aka Adjustments required for arm 2 
            clearance = calculateClearance (currentBlockHt, clearance, highestBar, highestUnfilledBar);

            // Ensure that arm2 is high enough to clear the source pile
            if ((sourceHt + 1) > clearance)
            {
                clearance = sourceHt + 1;
            }

            height = clearanceAdjustment(height, clearance); 


            // ### 9 ### - LOWER GRAPPLE AND DROP OFF
            //   Lower arm3 to allow the current block to be 'dropped off' 
            while ( (height - 1) - depth - currentBlockHt > barHeights[width-3] )  
            {
                r.lower();
                depth++;  // Current depth of arm 3 being decreased
            }
            // The height of currentBar increases by block just placed    
            barHeights[currentBar] = barHeights[currentBar] + currentBlockHt;

            // If this is higher than the highest bar, then increase the highest bar variable to reflect this
            if (barHeights[currentBar] > highestBar)
            {
                highestBar = barHeights[currentBar];
            }
            currentBar++;  // Shift to the next bar in the array barHeights[]
            fullColumns++; // Once Column 3-9 has a Block in it, increase the fullColumns Variable to reflect this



            r.drop();  // dropping the block
            currentBlock--;  // ID Number of Currently selected BLOCK within the blockHeights[] ARRAY
            currentBlockHt = 0;  // No Currently held block


            // ### 10 ### - RAISE GRAPPLE 
            //   Raising the grapple, if it is likely to collide with the highestUnfilledBar or the Source Pile, before restarting the main loop 
            if (highestUnfilledBar > sourceHt)
            {
                while (height - depth < (highestUnfilledBar + currentBlockHt + 1))
                {
                    r.raise();
                    depth--;
                } 
            }
            else if ((highestUnfilledBar < sourceHt))
            {                   
                while (height - depth < (sourceHt + 1))
                {
                    r.raise();
                    depth--;
                } 
            }
            
        } // Close main program (while (sourceHt != 0))

    } // public void controlMechanismForScenarioB

*/    

    /* ##############################################################################################
    ### THE FOLLOWING METHODS ARE USED IN MECHANISM B, BUT ARE NOT USED IN THE FINAL MECHANISM C, ###
    ### HENCE I HAVE MOVED THEM TO THE END OF THIS METHOD PURELY FOR DOCUMENTATION PURPOSES.      ###
    ###             AS THEY ARE NO LONGER NEEDED, I WOULD NORMALLY DELETE IT.                     ###                                                                ###
    #################################################################################################  */

/*


    // Calculates the height arm2 needs to be to clear the other objects while moving currentBlock, or to pickup the next block
    public int calculateClearance (int currentBlockHt, int clearance, int highestBar, int highestUnfilledBar)
    {
        
            if (highestBar > highestUnfilledBar)  // We don't need to go over the highest bar as we are filling columns 3 TO 9,   hence  3 THEN 4 THEN 5 etc
            {
                clearance = highestBar + 1;
            }
            else // Unsure of which bar is the highest, so need to be high enough to get over it
            {
                clearance = highestUnfilledBar + currentBlockHt + 1;  // ### POSSIBLE EXTRA MOVES WASTED HERE ###
            }
        
        return clearance;
    }


    // Calculates the height of the highest bar, by reading through the array and comparing each element 
    //   This is used both to calculate the highestBar in the array, and the highestUnfilledBar 
    //   This will ONLY parse through the Unfilled sections as currentBar will increase as the bars get filled 
    public int calculateHighestBar(int currentBar, int barHeights[])
    {
        int highestBar = 0;  // USED AS A THROW AWAY VARIABLE IN THIS FUNCTION

        while (currentBar < barHeights.length)   
        {                                               
          if (highestBar < barHeights[currentBar])
          {
            highestBar = barHeights[currentBar];
          }
          currentBar++;
        }
        return highestBar;
        // currentBar = 0;  // NO LONGER NEEDED AS THIS ISN'T A RETURN VALUE, AND DOESN'T AFFECT VARIABLE OF SAME NAME WITHIN MAIN FUNCTION
    }
   

    // Adjusts arm2 to height of the clearance variable previously calculated during calculateClearance above
    public int clearanceAdjustment(int height, int clearance)
    {
        if (height < clearance)
        {
            while (height < clearance) 
            {
                r.up();     
                height++;
            }
        }
        else 
        {
            while (height > clearance)
            {
                r.down();     
                height--;
            } 
        }   
        return height; 
*/  
    }   


   
    public void controlMechanismForScenarioC(int barHeights[], int blockHeights[])
    {
        /* ##########################################################################################
        #                 ALGORITHM DESIGN AND PROGRAMMING BY CAMERON WATT S3589163                 #
        #                                   Version 12 1st April                                    #
        #                         SOLUTION TO Part A + B + C EVEN SCENARIO X                        #
        #   THIS WILL SOLVE ALMOST ANY CONFIGURATION, I HAVE TESTED IT USING ALEX'S RANDOM FUNCTION #
        #  WHICH I HAVE INCLUDED IN THE robotTest.java.  THIS VERSION CAN ALSO ACHIEVE THE MINIMUM  #
        #                        MOVES FOR EVERY ONE OF THE INCLUDED TESTS 1-8                      #
        #############################################################################################  

          - - - -  A R M 2  - - - 
        |                         A
        |                         R
        A                         M
        R                  __     3
        M            __   |  |   __
        1   __    __|  |__|  |  |  |
        |  |  |__|           |  |  |

        ARM 3 AKA THE GRAPPLE (HOOK) ARM


        PROGRAM FLOW - TABLE OF CONTENTS

        ### 1 ### - DECLARE VARIABLES
        ### 2 ### - CALCULATE sourceHt
        ### 3 ### - CALCULATE highestBar

            MAIN PROGRAM LOOP WHILE (SOURCEHT > 0)
            {
            ### 4 ### - CALCULATE highestUnfilledBar
            ### 5 ### - RAISE ARM2 AND GRAPPLE READY FOR NEXT BLOCK 
            ### 6 ### - PICKUP NEXT BLOCK FROM SOURCE  
            ### 7 ### - RAISE ARM2 AND GRAPPLE READY FOR DROPOFF 
            ### 8 ### - CALCULATE AND MOVE TO TARGET COLUMN
            ### 9 ### - LOWER GRAPPLE AND DROP OFF
            }                        

        /* ### 1 ### DECLARE VARIABLES
            Declaration of all variable used within this program */
        int height = 2;        // Initial height of arm 1
        int width = 1;         // Initial width of arm 2  
        int depth = 0;         // Initial depth of arm 3

        int col1Ht = 0;        // Variable to show the height of Column 1
        int col2Ht = 0;        // Variable to show the height of Column 2
        int sourceHt = 0;      // Variable to show the height of the Source Pile

        int highestBar = 0;     // Stores value of the highest bar from the array barHeights[] (calculated during part 3)
        int highestUnfilledBar = 0;    // stores the height of the highest bar without any block on it
        int clearance = 0;      // TOTAL HEIGHT TO BE CLEARED,  ### will not INCLUDE THE +1 HEIGHT FOR ARM2 ###
        int targetColumn = 0;   // column to move to

        final int SOURCE_COLUMN = 10;     // Column number of the Source Pile

        int currentBar  = 0;    // ID Number of Currently selected BAR within the barHeights[] ARRAY
        int currentBlock = (blockHeights.length - 1);   // ID Number of Currently selected BLOCK within the blockHeights[] ARRAY
                                    // NOTE THE blockHeights[] array is displayed in reverse, as the first element is the block on the bottom of source pile


        /* ### 2 ### - CALCULATE sourceHt
            Calculating the sourceHt variable, by reading through the array and adding each element
            of the blockHeights array, one element at a time */ 
        for (int counter = 0; counter < blockHeights.length; counter++)
        {
            sourceHt = sourceHt + blockHeights[counter];
        }


        /* ### 3 ### - CALCULATE highestBar
            Calculate the height of the highest bar, by reading through the array and comparing each element */
        for (int counter = 0; counter < barHeights.length; counter++)
        {
            if (highestBar < barHeights[counter])
            {
                highestBar = barHeights[counter];
            }
        }


        // MAIN PROGRAM LOOP
        while (sourceHt != 0)
        {
            /* ### 4 ### - CALCULATE highestUnfilledBar 
                Re-Calculate the highestUnfilled bar within the barHeights array*/
            highestUnfilledBar = 0;     // Reset at the start of each main program loop 
            for (int counter = currentBar; counter < barHeights.length; counter++)
            {
                if (highestUnfilledBar < barHeights[counter])
                {
                    highestUnfilledBar = barHeights[counter];
                }
            } 


            /* ### 5 ### - RAISE ARM2 AND GRAPPLE READY FOR NEXT BLOCK */
            if (width < 3) // If the current position is currently column 1 or column 2
            {
                if (sourceHt < highestBar)
                {
                    clearance = highestBar;
                }
                else 
                {
                    clearance = sourceHt;
                }
            }
            else if (width >= 3) // If the current position is greater than column 3
            {
                if (sourceHt < highestUnfilledBar)
                {
                    clearance = highestUnfilledBar;
                }
                else
                {
                    clearance = sourceHt;
                }
            }

            // Raise arm2 if required
            while (height - 1 < clearance)
            {
                r.up();
                height++;
            }        
            
            // Raise grapple if required to match the clearance height
            while (height - depth - 1 < clearance) 
            {
                r.raise();
                depth--;
            }


            /* ### 6 ### - PICKUP NEXT BLOCK FROM SOURCE 
                Extend to source pile */
            while (width < SOURCE_COLUMN)
            {
                r.extend();
                width++; 
            }


            // Lower grapple arm to the top most block      
            while ((height - depth - 1) > sourceHt) //using position of grapple
            {
                r.lower();
                depth++;
            }

            // Pickup block
            r.pick();

            //reduce sourceHt
            sourceHt = sourceHt - blockHeights[currentBlock];


            /* ### 7 ### - RAISE ARM2 AND GRAPPLE READY FOR DROPOFF  
                only need to raise high enough to clear highest point between source and target column, if needed, it can be lowered later */
            if (blockHeights[currentBlock] == 3)
            {
                clearance = highestUnfilledBar + blockHeights[currentBlock];
            }
            else if (blockHeights[currentBlock] < 3)
            {
                clearance = highestBar + blockHeights[currentBlock];

                // as we are moving to or over column 2, we need to check its height against the current clearance
                if (clearance < col2Ht + blockHeights[currentBlock])
                {
                    clearance = col2Ht + blockHeights[currentBlock];
                }

                // if we need to go to column 1, we also need to check its height against the current clearance
                if ((clearance < col1Ht + blockHeights[currentBlock]) && (blockHeights[currentBlock] == 1))
                {
                    clearance = col1Ht + blockHeights[currentBlock];
                }
            }

            // IF ARM2 needs to be raised
            while (height - 1 < clearance) // arm2 needs to be high enough to place the next block
            {
                r.up();
                height++;
            }

            while (height - depth - 1 < clearance) // raise grapple to move above the clearance height
            {
                r.raise();
                depth--;
            }


            /* ### 8 ### - CALCULATE AND MOVE TO TARGET COLUMN */
            if (blockHeights[currentBlock] == 3)
            {
                targetColumn = currentBar + 3; // as current bar starts at 0, while the first bar is in column 3
            }
            else
            {
                targetColumn = blockHeights[currentBlock];
            }

            // Moving to the target column
            while (width > targetColumn)
            {
                r.contract();
                width--;
            }


            /* ### 9 ### - LOWER GRAPPLE AND DROP OFF  */
            switch(blockHeights[currentBlock])
            {
                case 1:
                {
                    while ((height - depth - 1 - blockHeights[currentBlock]) > col1Ht)
                    {
                        r.lower();
                        depth++;
                    }
                    col1Ht = col1Ht + blockHeights[currentBlock];   // increase height of col1Ht to show the new height
                    break;
                }
                case 2:
                {
                    while ((height - depth - 1 - blockHeights[currentBlock]) > col2Ht)
                    {
                        r.lower();
                        depth++;
                    }
                    col2Ht = col2Ht + blockHeights[currentBlock];   // increase height of col2Ht to show the new height
                    break;
                }
                case 3:
                {
                    while ((height - depth - 1 - blockHeights[currentBlock]) > barHeights[currentBar])
                    {
                        r.lower();
                        depth++;
                    }

                    barHeights[currentBar] = barHeights[currentBar] + blockHeights[currentBlock];   // increase height of current bar within the barHeights array to show the new height

                    // If this is higher than the highestBar, then increase the highestBar variable to reflect this
                    if (barHeights[currentBar] > highestBar)
                    {
                        highestBar = barHeights[currentBar];
                    }

                    currentBar++;   // increment to move to the next bar within the barHeights array when used next
                }
            }

            // Drop off block
            r.drop();
            currentBlock--;     // increment to move to the previous block within the blockHeights array when used next (as this array's first element is at bottom of source pile)

        }   // END Main program loop  while (sourceHt != 0)

    }  // END public void controlMechanismForScenarioC(int barHeights[], int blockHeights[])

} // Close class robotControl